﻿namespace DialogEd {
    public interface IDialogueActor {
        
        /// <summary>
        /// This is the name the actor is referred to in the dialog script
        /// </summary>
        public string Id { get; }
        
        /// <summary>
        /// This is the display name that is used for the this actor.
        /// </summary>
        public string ActorName { get; }
    }
}