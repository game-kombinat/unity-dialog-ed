﻿using System.Collections.Generic;
using System.Text;
using DialogEd.Model.Commands;
using UnityEngine;

namespace DialogEd {
    [CreateAssetMenu(menuName = "DialogEd/Commands/Debug Command")]
    public class DebugCommand : DialogueCommand {
        public override void Init(StoryInterpreter inInterpreter) {
            Debug.Log("Init Debug Command");
        }

        public override void Execute(List<CommandArgument> args) {
            Debug.Log("Execute Debug Command");
            if (args.Count == 0) {
                Debug.Log("\tNo Args");
                return;
            }
            
            StringBuilder cmds = new StringBuilder("\t");
            for (int i = 0; i < args.Count; i++) {
                
                if (args[i].argumentType == ArgumentType.Number) {
                    cmds.Append($"{args[i].intValue}");
                }
                else {
                    cmds.Append($"{args[i].stringValue}");
                }

                if (i < args.Count - 1) {
                    cmds.Append(", ");
                }
            }

            Debug.Log(cmds.ToString());
        }
    }
}