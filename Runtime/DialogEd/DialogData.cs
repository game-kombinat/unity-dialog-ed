﻿using System.Collections.Generic;

namespace DialogEd {
    public struct DialogData {
        public IDialogueActor dialogueActor;
        public string message;

        public Dictionary<int, string> choices;
    }
}