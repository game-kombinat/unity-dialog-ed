﻿using System;
using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    [Serializable]
    public enum EOperatorType {
        NoOp,
        Add,
        Sub,
        Mult,
        Div,
        Less,
        Greater,
        LessOrEqual,
        GreaterOrEqual,
        Equal,
        NotEqual,
        And,
        Or,
        Assignment,
        Literal,
        Variable,
    };

    public class BinOpNode : AstNode {
        [SerializeField]
        private EOperatorType opType = EOperatorType.NoOp;

        public void Init(ParsedToken t, AstNode inLeft, AstNode inRight) {
            Init(t);
            switch (t.tokenType) {
                case TokenType.Plus:
                    opType = EOperatorType.Add;
                    break;
                case TokenType.Minus:
                    opType = EOperatorType.Sub;
                    break;
                case TokenType.Slash:
                    opType = EOperatorType.Div;
                    break;
                case TokenType.Asterisk:
                    opType = EOperatorType.Mult;
                    break;

                case TokenType.Less:
                    opType = EOperatorType.Less;
                    break;
                case TokenType.Greater:
                    opType = EOperatorType.Greater;
                    break;
                case TokenType.LessOrEqual:
                    opType = EOperatorType.LessOrEqual;
                    break;
                case TokenType.GreaterOrEqual:
                    opType = EOperatorType.GreaterOrEqual;
                    break;
                case TokenType.Equal:
                    opType = EOperatorType.Equal;
                    break;
                case TokenType.NotEqual:
                    opType = EOperatorType.NotEqual;
                    break;

                case TokenType.And:
                    opType = EOperatorType.And;
                    break;
                case TokenType.Or:
                    opType = EOperatorType.Or;
                    break;
                case TokenType.SingleEqual:
                    opType = EOperatorType.Assignment;
                    break;
                case TokenType.NumberLiteral:
                    opType = EOperatorType.Literal;
                    break;
                case TokenType.Identifier:
                    opType = EOperatorType.Variable;
                    break;
                default:
                    opType = EOperatorType.NoOp;
                    Debug.LogWarning($"Received a token type {t.tokenType} which results in a NoOp situation in binary operator node.");
                    break;
            }

            left = inLeft;
            right = inRight;
            token.tokenType = TokenType.BinOp;
        }

        public int Evaluate(StoryInterpreter storyInterpreter) {
            switch (opType) {
                case EOperatorType.NoOp:
                    Debug.LogWarning("NoOp BinOp node in story found!");
                    return 0;
                case EOperatorType.Add:
                    return GetLeftValue(storyInterpreter) + GetRightValue(storyInterpreter);
                case EOperatorType.Sub:
                    return GetLeftValue(storyInterpreter) - GetRightValue(storyInterpreter);
                case EOperatorType.Mult:
                    return GetLeftValue(storyInterpreter) * GetRightValue(storyInterpreter);
                case EOperatorType.Div:
                    return GetLeftValue(storyInterpreter) / Mathf.Max(1, GetRightValue(storyInterpreter));
                case EOperatorType.Less:
                    return GetLeftValue(storyInterpreter) < GetRightValue(storyInterpreter) ? 1 : 0;
                case EOperatorType.Greater:
                    return GetLeftValue(storyInterpreter) > GetRightValue(storyInterpreter) ? 1 : 0;
                case EOperatorType.LessOrEqual:
                    return GetLeftValue(storyInterpreter) <= GetRightValue(storyInterpreter) ? 1 : 0;
                case EOperatorType.GreaterOrEqual:
                    return GetLeftValue(storyInterpreter) >= GetRightValue(storyInterpreter) ? 1 : 0;
                case EOperatorType.Equal: {
                    // covers the case `if someVar` and evaluates that to true or false
                    if (HasOnlyLeftHandSide()) {
                        return GetLeftValue(storyInterpreter) > 0 ? 1 : 0;
                    }

                    return GetLeftValue(storyInterpreter) == GetRightValue(storyInterpreter) ? 1 : 0;
                }

                case EOperatorType.NotEqual:
                    return GetLeftValue(storyInterpreter) != GetRightValue(storyInterpreter) ? 1 : 0;
                case EOperatorType.And: {
                    bool lboolAnd = GetLeftValue(storyInterpreter) > 0;
                    bool rboolAnd = GetRightValue(storyInterpreter) > 0;
                    return lboolAnd && rboolAnd ? 1 : 0;
                }

                case EOperatorType.Or: {
                    bool lboolOr = GetLeftValue(storyInterpreter) > 0;
                    bool rboolOr = GetRightValue(storyInterpreter) > 0;
                    return lboolOr || rboolOr ? 1 : 0;
                }

                case EOperatorType.Assignment:
                    return Assign(storyInterpreter);
                case EOperatorType.Literal:
                case EOperatorType.Variable:
                    return GetLeftValue(storyInterpreter);
                default:
                    return 0;
            }
        }

        private string GetIdentifierType() {
            if (!Statements || Statements.Token.tokenType != TokenType.Identifier) {
                return "";
            }
            var lval = (IdentifierNode)Statements;
            return lval.IdentifierType;
        }

        private string GetLeftIdentifier() {
            if (!Statements || Statements.Token.tokenType != TokenType.Identifier) {
                return "";
            }
            var lval = (IdentifierNode)Statements;
            return lval.IdentifierLabel;
        }
        
        private int Assign(StoryInterpreter interpreter) {
            interpreter.SetVariableValue(GetLeftIdentifier(), GetRightValue(interpreter), LeftIsGlobal(), GetIdentifierType());
            return 1;
        }

        private bool LeftIsGlobal() {
            if (!Statements || Statements.Token.tokenType != TokenType.Identifier) {
                return false;
            }
            var lval = (IdentifierNode)Statements;
            return lval.GlobalIdentifier;
        }

        private bool HasOnlyLeftHandSide() {
            return !right;
        }

        public int GetNodeValue(StoryInterpreter interpreter, AstNode node) {
            if (node.Token.tokenType == TokenType.Identifier) {
                var lval = (IdentifierNode)node;
                return interpreter.GetVariableValue(lval.IdentifierLabel, lval.GlobalIdentifier, lval.IdentifierType);
            }
            
            // note: we cannot process string literals here as ... you see, they are not numbers.
            if (node.Token.tokenType == TokenType.NumberLiteral) {
                var lval = (NumberLiteralNode)node;
                return lval.Value;
            }
    
            if (node.Token.tokenType == TokenType.BinOp) {
                var lval = (BinOpNode)node;
                return lval.Evaluate(interpreter);
            }
            return 0;
        }

        public int GetLeftValue(StoryInterpreter interpreter) {
            if (!Statements) {
                // this is not quite valid. A binop can be a single identifier but
                // it shouldn't be the left node that is missing 
                Debug.LogWarning("BinOp: Left Value is null!");
                return 0;
            }

            return GetNodeValue(interpreter, Statements);
        }
        
        public int GetRightValue(StoryInterpreter interpreter) {
            if (!right) {
                // this is a valid situation, actually. Since a binop node can represent a single identifier.
                return 0;
            }

            return GetNodeValue(interpreter, right);
        }
    }
}