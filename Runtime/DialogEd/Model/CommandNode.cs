﻿using System.Collections.Generic;
using DialogEd.Model.Commands;
using DialogEd.Parser;

namespace DialogEd.Model {
    public class CommandNode : AstNode {

        public string CommandName => ((IdentifierNode)Statements).IdentifierLabel;

        public List<CommandArgument> GetArgs(StoryInterpreter interpreter) {
            List<CommandArgument> args = new List<CommandArgument>();
            var arg = right;

            while (arg) {
                
                if (arg.IsLeaf) {
                    break;
                }
                
                CommandArgument interpretedArgument = new CommandArgument();
                
                if (arg.Left.Token.tokenType == TokenType.StringLiteral) {
                    interpretedArgument.argumentType = ArgumentType.String;
                    interpretedArgument.stringValue = ((StringLiteralNode)arg.Left).Value;
                }
                else {
                    interpretedArgument.argumentType = ArgumentType.Number;
                    interpretedArgument.intValue = ((BinOpNode)arg.Left).Evaluate(interpreter);
                }
                args.Add(interpretedArgument);
        
                arg = arg.Right;
            }
            
            return args;
        }
    }
}