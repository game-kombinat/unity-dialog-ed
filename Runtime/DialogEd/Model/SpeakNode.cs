﻿using DialogEd.Parser;

namespace DialogEd.Model {
    public class SpeakNode : AstNode {

        public string Speaker => ((IdentifierNode)left).IdentifierLabel;
        public string Text => ((StringLiteralNode)right).Value;
        
        public void Init(IdentifierNode speaker, StringLiteralNode text) {
            token = new ParsedToken() {
                tokenType = TokenType.Speech,
            };
            left = speaker;
            right = text;
        }
    }
}