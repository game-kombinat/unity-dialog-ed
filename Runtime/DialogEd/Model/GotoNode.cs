﻿using DialogEd.Parser;

namespace DialogEd.Model {
    public class GotoNode : AstNode {

        public string TargetThread => ((StringLiteralNode)left).Value;
        public void Init(ParsedToken t, StringLiteralNode threadName) {
            Init(t);
            left = threadName;
        }
    }
}