﻿using System.Collections.Generic;
using UnityEngine;

namespace DialogEd.Model {
    
    /// <summary>
    /// This represents the AST model of a story.
    /// This is created when importing story text assets.
    /// </summary>
    public class Story : ScriptableObject {
        [SerializeField]
        private List<ThreadNode> storyThreads;

        public void SetStoryThreads(List<ThreadNode> threads) {
            storyThreads = threads;
        }

        public ThreadNode GetStoryThread(string threadName) {
            for (int i = 0; i < storyThreads.Count; i++) {
                if (storyThreads[i].ThreadName == threadName) {
                    return storyThreads[i];
                }
            }
            
            return null;
        }
    }
}