﻿using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    public class ThreadNode : AstNode {
        [SerializeField]
        private string threadName;

        public string ThreadName => threadName;

        public override void Init(ParsedToken t) {
            base.Init(t);
            // Removes the <<<< part of the token and trims whitespaces.
            threadName = t.value.Substring(4).Trim();
        }
    }
}