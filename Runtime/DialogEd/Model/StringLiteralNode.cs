﻿using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    public class StringLiteralNode : AstNode {
        [SerializeField]
        private string value;

        public string Value => value;

        public void Init(ParsedToken t, string inValue) {
            Init(t);
            value = inValue;
        }
    }
}