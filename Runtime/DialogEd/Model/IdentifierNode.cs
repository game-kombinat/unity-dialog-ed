﻿using System;
using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    public class IdentifierNode : AstNode {
        [SerializeField]
        private string identifierLabel;

        // Used to uniquely identify this identifier.
        // Important, for one, for magic variables.
        [SerializeField]
        private string guid;

        [SerializeField]
        private bool globalIdentifier;
        
        public string Guid => guid;
        
        public string IdentifierType {
            get {
                if (right) {
                    return ((IdentifierNode)right).identifierLabel;
                }

                return "";
            }
        }

        public string IdentifierLabel => identifierLabel;

        public bool GlobalIdentifier => globalIdentifier;

        public override void Init(ParsedToken t) {
            base.Init(t);
            identifierLabel = t.value;
            guid = System.Guid.NewGuid().ToString();
            globalIdentifier = false;
        }
        
        public void Init(ParsedToken t, IdentifierNode identifierType) {
            Init(t);
            right = identifierType;
        }

        public void SetScope(bool isGlobal) {
            globalIdentifier = isGlobal;
        }
    }
}