﻿using System.Collections.Generic;
using DialogEd.Model.Commands;
using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    public class NumberLiteralNode : AstNode {
        [SerializeField]
        private int value;
        public int Value => value;

        public override void Init(ParsedToken t) {
            base.Init(t);
            int.TryParse(t.value, out value);
        }

        List<CommandArgument> GetArgs() {
            // todo: implement this
            return default;
            /*
             *  TArray<FCommandArgument> args;
                auto arg = right;
                while (arg) {
                    FCommandArgument interpretedArgument;
                    // There are commands that have no arguments, believe it or not.
                    if (arg->IsLeaf()) {
                        break;
                    }
                    
                    if (arg->left->token.tokenType == ETokenType::StringLiteral) {
                        interpretedArgument.argumentType = EArgumentType::String;
                        interpretedArgument.stringValue = static_cast<UStringLiteralNode*>(arg->left)->value;
                    }
                    else {
                        interpretedArgument.argumentType = EArgumentType::Number;
                        interpretedArgument.numberValue = static_cast<UBinOpNode*>(arg->left)->Evaluate(runner);
                    }
                    args.Add(interpretedArgument);
                    
                    arg = arg->right;
                }

                Algo::Reverse(args);
                return args;
             */
        }
    }
}