﻿using System.Collections.Generic;
using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    public class ChoiceNode : AstNode {

        [SerializeField]
        private string choiceLabel;

        public AstNode Branch => Statements;
        
        public override void Init(ParsedToken t) {
            base.Init(t);
            choiceLabel = t.value.Trim('"');
        }

        public Dictionary<int, string> GetChoices() {
            Dictionary<int, string> choices = new Dictionary<int, string>();
            var node = this;
            int nesting = 0;

            while (node) {
                choices.Add(nesting++, node.choiceLabel);
                node = (ChoiceNode)node.right;
            }
            return choices;
        }

        public ChoiceNode GetChoice(int choice) {
            int iterations = 0;
            var node = this;

            while (iterations < choice) {
                node = (ChoiceNode)node.right;
                ++iterations;
            }

            return node;
        }
    }
}