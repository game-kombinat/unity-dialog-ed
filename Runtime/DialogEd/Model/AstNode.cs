﻿using System;
using System.Text;
using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Model {
    [Serializable] // gotta be a SO because of unitys nesting limitations and we gotta handle references manually.
    public class AstNode : ScriptableObject {
        // tree
        [SerializeField]
        protected AstNode left;
        [SerializeField]
        protected AstNode right;
        
        // data
        [SerializeField]
        protected ParsedToken token = new() {tokenType = TokenType.Node};
        
        // tree-wise labels
        public AstNode Left {
            get => left;
            set => left = value;
        }

        public AstNode Right {
            get => right;
            set => right = value;
        }

        // semantically helpful labels
        public AstNode Statements => left;
        public AstNode Next => right;

        public bool IsLeaf => !Statements && !right;
        public ParsedToken Token => token;

        public virtual void Init(ParsedToken t) {
            token = t;
        }
        
        public override string ToString() {
            return $"{{\n{ToString(0)}\n}}";
        }

        protected string ToString(int indent) {
            StringBuilder output = new StringBuilder();
            StringBuilder indentation = new StringBuilder();
            for (int i = 0; i < indent; ++i) {
                indentation.Append("\t");
            }
            var l = Statements ? Statements.ToString(indent + 1) : "{}";
            var r = right ? right.ToString(indent + 1) : "{}";
            output.Append($"{{\n{indentation}\"type\": \"{token.tokenType}\",\n{indentation}\"left\": {l},\n{indent}\"right\": {r},\n{indent}}}");
            return output.ToString();
        }
    }
}