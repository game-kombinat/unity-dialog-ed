﻿using System;

namespace DialogEd.Model.Commands {

    [Serializable]
    public enum ArgumentType {
        String,
        Number
    }
    [Serializable]
    public struct CommandArgument {
        public ArgumentType argumentType;

        public int intValue;

        public string stringValue;
    }
}