﻿using System.Collections.Generic;
using DialogEd.Data;

namespace DialogEd.Model.Commands {
    /// <summary>
    /// Executes any kind of logic. This is resolved at runtime.
    /// </summary>
    public abstract class DialogueCommand : InitializableData {
        public abstract void Execute(List<CommandArgument> args);
    }
}