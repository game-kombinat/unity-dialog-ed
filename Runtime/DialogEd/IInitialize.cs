namespace DialogEd {
    public interface IInitialize {
        public void Init(StoryInterpreter inInterpreter);
    }
}