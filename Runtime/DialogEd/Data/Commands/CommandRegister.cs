using System;
using System.Collections.Generic;
using System.Linq;
using DialogEd.Model.Commands;
using UnityEngine;

namespace DialogEd.Data.Commands {
    [CreateAssetMenu(menuName = "DialogEd/Data/Command Register", fileName = "Command Register")]
    public class CommandRegister : StoryInterpreterRegister<string, DialogueCommand> {
        [Serializable]
        public struct Entry {
            [SerializeField]
            public string commandName;

            [SerializeField]
            public DialogueCommand logic;
        }

        [SerializeField] 
        private List<Entry> commandMap;
        
        protected override DialogueCommand GetNew(string key) {
            return commandMap.FirstOrDefault(x => x.commandName == key).logic;
        }
    }
}