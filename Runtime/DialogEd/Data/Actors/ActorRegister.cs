﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DialogEd.Data.Actors {
    
    [CreateAssetMenu(menuName = "DialogEd/Data/Actor Register", fileName = "Actor Register")]
    public class ActorRegister : ScriptableObject {
        [Serializable]
        protected struct Entry {
            [SerializeReference, SubclassSelector]
            public ActorLookup lookup;
        }
        
        [SerializeField]
        private List<Entry> registrySources;
        
        private readonly Dictionary<string, IDialogueActor> resolvedActors = new();

        private readonly List<string> failedLookups = new();

        public void Init(StoryInterpreter interpreter) {
            for (int i = 0; i < registrySources.Count; i++) {
                registrySources[i].lookup.Init(interpreter.GetWorld());
            }
        }
        public IDialogueActor Get(string id) {
            if (resolvedActors.TryGetValue(id, out var actor)) {
                return actor;
            }

            if (failedLookups.Contains(id)) {
                return null;
            }

            for (int i = 0; i < registrySources.Count; i++) {
                var thing = registrySources[i].lookup.Find(id);
                if (thing == null) {
                    continue;
                }
                resolvedActors.Add(id, thing);
                return thing;
            }
            
            // this is more expensive than I like and gets called potentially a lot
            // so mark failed lookups as that.
            failedLookups.Add(id);

            return null;
        }

    }
}