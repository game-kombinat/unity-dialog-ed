﻿using Broilerplate.Core;

namespace DialogEd.Data.Actors {
    /// <summary>
    /// Provides means to look up actors from a source.
    /// </summary>
    public abstract class ActorLookup {
        protected World world;
        
        public virtual void Init(World inWorld) {
            world = inWorld;
        }

        public abstract IDialogueActor Find(string id);
    }
}