﻿using System;

namespace DialogEd.Data.Actors {
    /// <summary>
    /// Handles actor lookup by the currently loaded world.
    /// </summary>
    [Serializable]
    public class ActorWorldLookup : ActorLookup {
        
        public override IDialogueActor Find(string id) {
            var actors = world.FindTypesInActors<IDialogueActor>();
            for (int i = 0; i < actors.Count; i++) {
                if (actors[i].Id == id) {
                    return actors[i];
                }
            }
            return null;
        }
    }
}