using UnityEngine;

namespace DialogEd.Data {
    public abstract class InitializableData : ScriptableObject, IInitialize {
        public abstract void Init(StoryInterpreter inInterpreter);
    }
}