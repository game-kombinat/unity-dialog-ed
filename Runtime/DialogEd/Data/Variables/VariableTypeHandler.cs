﻿
using UnityEngine;

namespace DialogEd.Data.Variables {
    /// <summary>
    /// Handles the details of variable types retrieval and storage.
    /// </summary>
    public abstract class VariableTypeHandler : InitializableData {
        public abstract int GetValue(string variableName, bool global);

        public abstract void SetValue(string variableName, int value, bool global);
    }
}