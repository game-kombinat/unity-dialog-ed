﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DialogEd.Data.Variables {
    /// <summary>
    /// Registers different variable types.
    /// Duh.
    /// These can then be used and referenced in the dialogue script like so: item:someItemVar
    /// </summary>
    [CreateAssetMenu(menuName = "DialogEd/Data/Variable Type Register", fileName = "Variable Type Register")]
    public class VariableTypeRegister : StoryInterpreterRegister<string, VariableTypeHandler> {
        [Serializable]
        public struct Entry {
            public string typeName;
            
            public VariableTypeHandler handler;
        }

        [SerializeField]
        private List<Entry> register;

        protected override VariableTypeHandler GetNew(string key) {
            return register.FirstOrDefault(x => x.typeName == key).handler;
        }
    }
}