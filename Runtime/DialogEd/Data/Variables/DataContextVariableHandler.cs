﻿using System;

namespace DialogEd.Data.Variables {
    /// <summary>
    /// This is a reference implementation.
    /// It's not actually used to retrieve the default variable types from data context.
    /// But this is how you can make something like this.
    /// </summary>
    public class DataContextVariableHandler : VariableTypeHandler {
        private StoryInterpreter interpreter;
        
        public override int GetValue(string variableName, bool global) {
            return interpreter.LocalDataContext.GetInt(variableName);
        }

        public override void SetValue(string variableName, int value, bool global) {
            interpreter.LocalDataContext.Set(variableName, value);
        }

        public override void Init(StoryInterpreter inInterpreter) {
            // note: we take the data context from the runner because it can be overridden per running story.
            // otherwise we can't know which is active.
            interpreter = inInterpreter;
        }
    }
}