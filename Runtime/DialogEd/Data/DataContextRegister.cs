﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Broilerplate.Data;
using UnityEditor;
using UnityEngine;

namespace DialogEd.Data {
    [CreateAssetMenu(menuName = "DialogEd/Data/Data Context Register", fileName = "Data Context Register")]
    public class DataContextRegister : ScriptableObject {
        
        [Serializable]
        protected struct Entry {
            public string levelName;
            public DataContext dataContext;
        }
        
        [SerializeField]
        private DataContext globalDataContext;
        
        [SerializeField]
        private DataContext magicVars;

        private DataContext globalRuntimeDc;
        private DataContext runtimeMagicVars;

        private readonly Dictionary<string, DataContext> runtimeLevelDcs = new();

        public DataContext GlobalDataContext {
            get {
                if (!globalRuntimeDc) {
                    globalRuntimeDc = Instantiate(globalDataContext);
                }

                return globalRuntimeDc;
            }
        }

        public DataContext MagicVars {
            get {
                if (!runtimeMagicVars) {
                    runtimeMagicVars = Instantiate(magicVars);
                }

                return runtimeMagicVars;
            }
        }
        
        

        public DataContext Get(string level) {
            if (runtimeLevelDcs.TryGetValue(level, out var dc)) {
                return dc;
            }

            dc = CreateInstance<DataContext>();
            runtimeLevelDcs[level] = dc;

            return dc;
        }
        
        #if UNITY_EDITOR
        public void CreateSetup(string targetFolder) {
            globalDataContext = CreateInstance<DataContext>();
            globalDataContext.name = $"{name} Global";
            AssetDatabase.CreateAsset(globalDataContext, Path.Combine(targetFolder, $"{globalDataContext.name}.asset"));
            
            magicVars = CreateInstance<DataContext>();
            magicVars.name = $"{name} Magic Vars";
            AssetDatabase.CreateAsset(magicVars, Path.Combine(targetFolder, $"{magicVars.name}.asset"));
        }
        #endif
    }
}