﻿using System.Collections;
using Broilerplate.Core;
using Broilerplate.Tools;
using DialogEd.Model;
using NaughtyAttributes;
using UnityEngine;

namespace DialogEd {
    /// <summary>
    /// Can run any given story for debugging purposes.
    /// </summary>
    public class DebugStoryRunner : Actor {
        [SerializeField]
        private Story storyAsset;

        [SerializeField]
        private string threadName;

        [SerializeField]
        private int defaultChoice = 1;

        [Button]
        public void RunStory() {
            CoroutineJobs.StartJob(RunStoryRoutine());
        }

        private IEnumerator RunStoryRoutine() {
            var interpreter = GetWorld().GetSubsystem<StoryInterpreter>();
            interpreter.StartThread(storyAsset, threadName);
            DialogData data = new DialogData();
            while (interpreter.HasNext()) {
                var retval = interpreter.GetCurrent(ref data);
                if (retval == RunnerState.Ok) {
                    Debug.Log($"{data.dialogueActor.ActorName}: {data.message}");
                }
                

                if (retval == RunnerState.Choices) {
                    Debug.Log($"{data.dialogueActor.ActorName}: {data.message}");
                    interpreter.NextWithChoice(defaultChoice);
                }
                else {
                    interpreter.Next();
                }
                yield return null;
            }
            Debug.Log("Thread done.");
        }
    }
}