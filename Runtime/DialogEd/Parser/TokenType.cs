﻿using System;

namespace DialogEd.Parser {
    [Serializable]
    public enum TokenType {
        Invalid,
        // controls
        OpenStory, // >>>>
        CloseStory, // <<<<
        Comment, // // match everything from here to end of line as comment, and discard
        CommentMultiLine, // // match everything from here to end of line as comment, and discard

        // statements
        If, // if
        Else, // else
        Endif, // endif
        Goto, // goto "story thread label"
        BeginBranching, // **** followed by a speak token
        EndBranching, // **** followed by a speak token
        Branch, // -- followed by a text token
        Command, // -> followed by an identifier that is a function name
        Colon, // Speaker Name: followed by a body of text
        Comma, // separator between command arguments 

        // Operators
        And, // &&
        Or, // ||
        Greater, // >
        Less, // <,
        Equal, // ==
        GreaterOrEqual, // >=
        LessOrEqual, // <=
        NotEqual, // !=
        Plus,  // +
        Minus, // -
        Slash, // /
        Asterisk, // *
        SingleEqual, // =
        PlusPlus, // ++
        MinusMinus, // --

        // Expressions (when found as part of a speak statement, these should be interpreted as text)
        Identifier, // [a-zA-Z_]+
        NumberLiteral, // only [0-9] - the underlying data context works in integers only so we don't need other literals or values.
        Whitespace, // we match for whitespace because it has no meaning in its own and can only be part of some other token or get discarded.
    
        StringLiteral, // anything between two "
        LParen,
        RParen,

        // Meta token types, not part of the actual syntax.
        Node,
        Speech,
        BinOp,
        Global, // address identifier values in global scope where applicable
    }
}