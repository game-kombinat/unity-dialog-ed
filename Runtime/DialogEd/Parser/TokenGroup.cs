﻿using System.Collections.Generic;

namespace DialogEd.Parser {
    // This is a class because structs don't do inline defaults and I am lazy.
    public class TokenGroup {
        public List<ParsedToken> tokens = new();
    }
}