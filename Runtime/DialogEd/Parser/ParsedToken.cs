﻿using System;

namespace DialogEd.Parser {
    [Serializable]
    public struct ParsedToken {
        public TokenType tokenType;

        public string value;

        public int startIndex;
        public int endIndex;
        public int precedence;
    }
}