﻿using System.Collections.Generic;
using System.Linq;

namespace DialogEd.Parser {
    public class Tokenizer {
        // This makes the syntax happen. It's the Syntax make-it-happener.
        private static List<TokenDefinition> TokenDefinitions = new List<TokenDefinition>() {
            new (TokenType.OpenStory, ">>>>\\s*.+", 3),
            new (TokenType.CloseStory, "<<<<", 3),
            new (TokenType.Comment, "//.*", 4),
            new (TokenType.CommentMultiLine, "\\/\\*[^*]*\\*+(?:[^/*][^*]*\\*+)*\\/", 4),/* */
            new (TokenType.If, "if", 4),
            new (TokenType.Else, "else", 4),
            new (TokenType.Endif, "endif", 4),
            new (TokenType.Goto, "==>", 5), // conflicts with =>
            new (TokenType.BeginBranching, "choice", 4),
            new (TokenType.EndBranching, "endchoice", 4),
            new (TokenType.Branch, "=>", 4), // conflicts with =
            new (TokenType.Command, "->", 4),
            new (TokenType.Colon, ":", 1),
            new (TokenType.Comma, ",", 1),
            new (TokenType.And, "&&"),
            new (TokenType.Or, "\\|\\|"),
            new (TokenType.Greater, ">", 2), // conflicts with open/close story
            new (TokenType.Less, "<", 2), // conflicts with open/close story
            new (TokenType.Equal, "==", 3), // conflicts with =
            new (TokenType.GreaterOrEqual, ">=", 3),
            new (TokenType.LessOrEqual, "<=", 3),
            new (TokenType.NotEqual, "!=", 3),
            new (TokenType.Plus, "\\+", 3),
            new (TokenType.Minus, "\\-", 3),
            new (TokenType.Slash, "\\/", 3),
            new (TokenType.Asterisk, "\\*", 3),
            new (TokenType.SingleEqual, "\\=", 3),
            new (TokenType.PlusPlus, "\\+\\+", 4),
            new (TokenType.MinusMinus, "--", 4),
            new (TokenType.Identifier, "[a-zA-Z_$][a-zA-Z_$0-9]+", 3),
            new (TokenType.NumberLiteral, "\\d+", 3),
            new (TokenType.StringLiteral, "\"[^\"]*\"", 2),
            new (TokenType.LParen, "\\(", 2),
            new (TokenType.RParen, "\\)", 2),
            new (TokenType.Global, "global", 4), // 2 because it could be evaluated as identifier but it's not
        };
        private List<ParsedToken> parsedTokens;


        public List<ParsedToken> Tokenize(string input) {
            parsedTokens = new List<ParsedToken>();
            
            // Generate every possible token in the source text. Make a nice hot token soup from it.
            // Yummy!
            List<ParsedToken> tokenSoup = new List<ParsedToken>();
            for (int i = 0; i < TokenDefinitions.Count; i++) {
                tokenSoup.AddRange(TokenDefinitions[i].FindMatches(input));
            }

            // Group tokens by their start index (same starting index deals with same text piece)
            Dictionary<int, TokenGroup> groupedTokens = new Dictionary<int, TokenGroup>();
            for (int i = 0; i < tokenSoup.Count; i++) {
                var t = tokenSoup[i];
                if (!groupedTokens.ContainsKey(t.startIndex)) {
                    groupedTokens.Add(t.startIndex, new TokenGroup());
                }
                groupedTokens[t.startIndex].tokens.Add(t);
            }
            
            // Make sure we run through the matches in the order of their appearance.
            // This way we end up with a valid sequence of tokens in the end for the AST to run through.
            var sortedGroupIndices = groupedTokens.Keys.ToList();
            sortedGroupIndices.Sort((a, b) => a.CompareTo(b));

            ParsedToken lastToken = default;
            for (int i = 0; i < sortedGroupIndices.Count; i++) {
                int index = sortedGroupIndices[i];
                int bestPrecedence = groupedTokens[index].tokens.Max(x => x.precedence);
                // note: if we have multiple with same precedence the match order will be making the decision.
                var token = groupedTokens[index].tokens.FirstOrDefault(x => x.precedence == bestPrecedence);
                if (lastToken.tokenType != TokenType.Invalid && token.startIndex < lastToken.endIndex) {
                    // That's a derp. we went backwards.
                    continue;
                }

                lastToken = token;
                parsedTokens.Add(token);
            }
            
            return parsedTokens;
        }
    }
}