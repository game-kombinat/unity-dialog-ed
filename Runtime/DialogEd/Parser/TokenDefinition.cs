﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DialogEd.Parser {
    /// <summary>
    /// This is configuration data that the Tokenizer uses
    /// to create ParsedToken structs from.
    /// </summary>
    public struct TokenDefinition {
        public TokenType tokenType;
        public string matchPattern;
        public int precedence;

        public TokenDefinition(TokenType type, string pattern) {
            tokenType = type;
            matchPattern = pattern;
            precedence = 10;
        }
        
        public TokenDefinition(TokenType type, string pattern, int prec) {
            tokenType = type;
            matchPattern = pattern;
            precedence = prec;
        }

        public List<ParsedToken> FindMatches(string input) {
            var matches = Regex.Matches(input, matchPattern);
            var parsedTokens = new List<ParsedToken>();
            for (int i = 0; i < matches.Count; i++) {
                var match = matches[i];
                    parsedTokens.Add(new ParsedToken() {
                        tokenType = tokenType,
                        value = match.Value,
                        startIndex = match.Index,
                        endIndex = match.Index + match.Length,
                        precedence = precedence
                    });
            }
            return parsedTokens;
        }
    }
}