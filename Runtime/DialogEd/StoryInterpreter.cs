﻿using System.Collections.Generic;
using Broilerplate.Core;
using Broilerplate.Core.Subsystems;
using Broilerplate.Data;
using DialogEd.Data;
using DialogEd.Data.Actors;
using DialogEd.Data.Commands;
using DialogEd.Data.Variables;
using DialogEd.Model;
using DialogEd.Parser;
using NaughtyAttributes;
using UnityEngine;

#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace DialogEd {
    public enum RunnerState {
        // All is well, the dialog data can be used
        Ok,

        // When attempting to call Next() when we're at a choice node
        NeedNext,

        // dialog data contains choices
        Choices,

        // story is through.
        Done
    }

    [CreateAssetMenu(menuName = "DialogEd/Story Interpreter", fileName = "Story Interpreter")]
    public class StoryInterpreter : WorldSubsystem {
        [SerializeField]
        private CommandRegister commandRegister;

        [SerializeField]
        private VariableTypeRegister variableTypeRegister;

        [SerializeField]
        private ActorRegister actorRegister;

        [SerializeField]
        private DataContextRegister dataContextRegister;

        /// <summary>
        /// Scene local data context.
        /// </summary>
        public DataContext LocalDataContext => dataContextRegister.Get(LevelManager.ActiveScene.name);

        /// <summary>
        /// Global data context valid everywhere.
        /// </summary>
        public DataContext GlobalDataContext => dataContextRegister.GlobalDataContext;

        public string ThreadName => threadNode ? threadNode.ThreadName : "None";

        public ActorRegister ActorRegister => actorRegister;

        /// <summary>
        /// The root node of the current story
        /// </summary>
        private ThreadNode threadNode;

        /// <summary>
        /// Currently processing base node.
        /// Tree consists of a number of nodes connected by their right-side children,
        /// Where the left side is the actual execution in that node.
        /// </summary>
        private AstNode currentNode;

        /// <summary>
        /// Return point stack when branches end.
        /// </summary>
        private Stack<AstNode> branchNodeStack = new();

        /// <summary>
        /// The story asset that we're currently running
        /// </summary>
        private Story storyAsset;

        private List<IDialogueActor> actorsInActiveThread = new();
        
        #if UNITY_EDITOR
        
        [Button]
        public void CreateSetup() {
            var path = AssetDatabase.GetAssetPath(this);
            var targetFolder = Path.GetDirectoryName(path);
            var interpreterName = name;
            Debug.Log(path);
            Debug.Log(targetFolder);
            Debug.Log(interpreterName);

            if (targetFolder == null) {
                Debug.LogError("Cannot determine target folder");
                return;
            }
            
            // create command register
            var cmdr = CreateInstance<CommandRegister>();
            cmdr.name = $"{interpreterName} Command Register";
            AssetDatabase.CreateAsset(cmdr, Path.Combine(targetFolder, $"{cmdr.name}.asset"));
            commandRegister = cmdr;
            
            // create VTR
            var vtr = CreateInstance<VariableTypeRegister>();
            vtr.name = $"{interpreterName} Var Type Register";
            AssetDatabase.CreateAsset(vtr, Path.Combine(targetFolder, $"{vtr.name}.asset"));
            variableTypeRegister = vtr;

            // create actor register
            var ar = CreateInstance<ActorRegister>();
            ar.name = $"{interpreterName} Actor Register";
            AssetDatabase.CreateAsset(ar, Path.Combine(targetFolder, $"{ar.name}.asset"));
            actorRegister = ar;
            
            // create DC register
            var dcr = CreateInstance<DataContextRegister>();
            dcr.name = $"{interpreterName} Data Context Register";
            AssetDatabase.CreateAsset(dcr, Path.Combine(targetFolder, $"{dcr.name}.asset"));
            dataContextRegister = dcr;
            dataContextRegister.CreateSetup(targetFolder);
            
        }
        
        #endif

        public override void LateBeginPlay() {
            base.LateBeginPlay();
            commandRegister.Init(this);
            variableTypeRegister.Init(this);
            actorRegister.Init(this);
        }

        public bool HasNext() {
            return (currentNode && currentNode.Right) || branchNodeStack.Count > 0;
        }

        public bool HandleIfElseBranching() {
            // left is expression
            // right is node with: left: true branch, right: false branch
            var ifNode = (BinOpNode)currentNode.Left.Left;
            var ifBranches = currentNode.Left.Right;

            if (!ifNode || !ifBranches) {
                Debug.LogError("Expected binop node for if branch. didn't get any. this should not go through the parser!");
                return false;
            }

            branchNodeStack.Push(currentNode);
            if (ifNode.Evaluate(this) > 0) {
                Debug.Log("Entering IF");
                currentNode = ifBranches.Left;
            }
            else {
                // The else branch is optional, after all.
                if (ifBranches.Right) {
                    Debug.Log("Entering ELSE");
                    currentNode = ifBranches.Right;
                }
                else {
                    Debug.Log("Returning to outer scope");
                    currentNode = branchNodeStack.Pop();
                }
            }

            return true;
        }

        /// <summary>
        /// Runs branches, if/else switches, diverts, BinOps and commands.
        /// Basically anything that's not a speech node.
        /// </summary>
        public void HandleScriptLogic() {
            // Check if it's an if token. And we can have nested ifs in sequence which is why this is a while
            while (currentNode.Left.Token.tokenType == TokenType.If) {
                HandleIfElseBranching();
            }

            // check if it's a goto / divert token
            if (currentNode.Left.Token.tokenType == TokenType.Goto) {
                Debug.Log("Found divert.");

                var divert = (GotoNode)currentNode.Left;
                var thread = storyAsset.GetStoryThread(divert.TargetThread);
                if (thread) {
                    branchNodeStack.Push(currentNode);
                    currentNode = thread;
                    HandleScriptLogic();
                    return;
                }

                Debug.LogError($"Divert to story thread {divert.TargetThread} not possible. No such thread in story asset {storyAsset.name}");
                return;
            }

            // check if it's a branching token
            if (currentNode.Left.Token.tokenType == TokenType.BeginBranching) {
                // left: speech node and
                // right: choice node
                // on choice node we get:
                // left: branch
                // right: next choice (if any)
                // and it has a label field for the choice on the left

                // we push the current node, so that when whatever chosen branch is finished,
                // we end up back here and can continue.

                // There is nothing else we need to do here.
                Debug.Log("Found choice.");
                branchNodeStack.Push(currentNode);
            }

            if (currentNode.Left.Token.tokenType == TokenType.BinOp) {
                var binOp = (BinOpNode)currentNode.Left;
                if (binOp) {
                    // this would be an assignment at this point.
                    // technically it could be any binop but it would make no sense.
                    binOp.Evaluate(this);
                }
            }

            if (currentNode.Left.Token.tokenType == TokenType.Command) {
                // command node:
                // left: command name
                // right: arg:      left:   binop
                //                  right:  next arg
                var cmdNode = (CommandNode)currentNode.Left;
                if (commandRegister) {
                    var cmd = commandRegister.Get(cmdNode.CommandName);
                    if (cmd) {
                        cmd.Execute(cmdNode.GetArgs(this));
                    }
                }
            }
        }

        /// <summary>
        /// Advances the AST to the right until we hit a node that has
        /// logic on its left child. Execute all other logic that we find on the way there.
        /// </summary>
        public void AdvanceAst() {
            // right is always a node that has
            // left: its logic
            // right: next node
            // this creates a chain of commands where going down right goes forward.

            // first, enqueue next node.
            if (!currentNode.Right) {
                currentNode = branchNodeStack.Pop();
                currentNode = currentNode.Right;
            }
            else {
                currentNode = currentNode.Right;
                // Gotta be a while here. If we reach the end of a branch that was nested, the next node might be an end node.
                while (currentNode.IsLeaf && branchNodeStack.Count > 0) {
                    currentNode = branchNodeStack.Pop();
                    currentNode = currentNode.Right;
                }
            }

            if (!currentNode || currentNode.IsLeaf && branchNodeStack.Count == 0) {
                // This is the end. But it's okay.
                currentNode = null;
                return;
            }

            HandleScriptLogic();
        }

        public void RunToNextSpeechNode() {
            while (currentNode || branchNodeStack.Count > 0) {
                AdvanceAst();
                if (!currentNode) {
                    break;
                }

                var currentType = currentNode.Statements.Token.tokenType;
                if (currentType == TokenType.Speech || currentType == TokenType.BeginBranching) {
                    break;
                }
            }
        }

        public RunnerState GetCurrent(ref DialogData dialogData) {
            if (currentNode.Statements.Token.tokenType == TokenType.Speech) {
                var speech = (SpeakNode)currentNode.Left;
                if (actorRegister) {
                    var actor = actorRegister.Get(speech.Speaker);
                    if (actor != null) {
                        dialogData.dialogueActor = actor;
                    }
                    else {
                        Debug.LogError($"Could not find an actor via the actor register: {speech.Speaker}");
                    }
                }

                dialogData.message = speech.Text;
                dialogData.choices?.Clear();
                return RunnerState.Ok;
            }

            if (currentNode.Statements.Token.tokenType == TokenType.BeginBranching) {
                var speech = (SpeakNode)currentNode.Statements.Left;
                var choice = (ChoiceNode)currentNode.Statements.Right;
                dialogData.choices = choice.GetChoices();
                if (actorRegister) {
                    var actor = actorRegister.Get(speech.Speaker);
                    dialogData.dialogueActor = actor;
                    if (actor == null) {
                        Debug.LogError($"Could not find an actor via the actor register: {speech.Speaker}");
                    }
                }

                dialogData.message = speech.Text;
                return RunnerState.Choices;
            }

            return RunnerState.NeedNext;
        }

        public RunnerState Next() {
            RunToNextSpeechNode();
            if (!currentNode) {
                return RunnerState.Done;
            }

            return RunnerState.Ok;
        }

        public RunnerState NextWithChoice(int choice) {
            // todo: This comment seems to be incorrect at this point as we
            // actually iterate over the choices internally in the choice node.
            
            // This here is a little special.
            // When the last next() returned choices, we didn't advance the current node.
            // so we're still on the choice node that has:
            // left: speech
            // right: first choice
            // and choice has:
            //      left: choice branch
            //      right: next choice
            // so in order to get the chosen branch we iterate x times over the choice node
            // and set currentNode to that and then call and return Next().
            // When this branch ends, the branchNodeStack pops and we can continue from there the root of the choice node where right is node after the branch.
            // This works on infinite amounts of nesting levels (in theory)
            var choiceNode = (ChoiceNode)currentNode.Statements.Right;
            choiceNode = choiceNode.GetChoice(choice);
            currentNode = choiceNode.Branch;
            return RunnerState.Ok;
        }

        public void StartThread(Story asset, string threadName) {
            if (!asset) {
                Debug.LogError($"No story asset given to run thread {threadName} from!");
                return;
            }

            PrepareAndRunStory(asset, threadName);
        }

        private void PrepareAndRunStory(Story asset, string threadName) {
            
            var newThread = asset.GetStoryThread(threadName);
            if (!newThread) {
                Debug.LogError($"There is no thread {threadName} in the story {asset.name}");
                return;
            }

            storyAsset = asset;
            threadNode = newThread;
            currentNode = newThread;
            SetVariableValue("__ran", GetVariableValue("__ran", false) + 1, false); // This is a magic var with special handling. scope doesn't have any effect
            HandleScriptLogic();
        }

        #region data context

        public int GetVariableValue(string varName, bool global, string type = null) {
            // todo: find a nicer way to implement magic vars
            // MAGIC CONSTANT!
            if (varName == "__ran") {
                var nodeName = $"__ran_{threadNode.ThreadName}";
                return dataContextRegister.MagicVars.GetInt(nodeName);
            }
            
            if (string.IsNullOrWhiteSpace(type)) {
                return (global ? GlobalDataContext : LocalDataContext).GetInt(varName);
            }

            return variableTypeRegister.Get(type)?.GetValue(name, global) ?? 0;
        }

        public void SetVariableValue(string varName, int value, bool global, string type = null) {
            // todo: find a nicer way to implement magic vars
            // MAGIC CONSTANT!
            if (varName == "__ran") {
                var nodeName = $"__ran_{threadNode.ThreadName}";
                dataContextRegister.MagicVars.Set(nodeName, value);
                return;
            }
            
            if (string.IsNullOrWhiteSpace(type)) {
                (global ? GlobalDataContext : LocalDataContext).Set(varName, value);
            }
            else {
                variableTypeRegister.Get(type)?.SetValue(varName, value, global);
            }

            
        }

        #endregion
    }
}