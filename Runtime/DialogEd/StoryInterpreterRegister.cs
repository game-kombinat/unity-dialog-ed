using System.Collections.Generic;
using DialogEd.Data;
using UnityEngine;

namespace DialogEd {
    public abstract class StoryInterpreterRegister<TKey, TValue> : ScriptableObject where TValue : InitializableData {
        
        protected Dictionary<TKey, TValue> instances = new();
        protected StoryInterpreter interpreter;

        public virtual void Init(StoryInterpreter inInterpreter) {
            interpreter = inInterpreter;
            Clear();
        }
        
        public TValue Get(TKey commandName) {
            if (instances.TryGetValue(commandName, out var obj)) {
                return obj;
            }

            var logic = GetNew(commandName);
            if (logic) {
                obj = Instantiate(logic);
                obj.Init(interpreter);
                instances[commandName] = obj;

                return obj;
            }
            Debug.LogWarning($"Register {name} does not contain a definition for {commandName}.");
            return null;
        }
        
        protected void Clear() {
            foreach (var kvp in instances) {
                if (kvp.Value) {
                    Destroy(kvp.Value);
                }
            }
            
            instances.Clear();
        }


        protected abstract TValue GetNew(TKey key);

    }
}