﻿using System.IO;
using DialogEd.Editor.DialogEd.Model;
using DialogEd.Model;
using DialogEd.Parser;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace DialogEd.Editor.DialogEd {
    [ScriptedImporter(1, "story")]
    public class StoryAssetImporter : ScriptedImporter{
        public override void OnImportAsset(AssetImportContext ctx) {
            string input = File.ReadAllText(ctx.assetPath);
            string fileName = Path.GetFileName(ctx.assetPath);
            var story = ScriptableObject.CreateInstance<Story>();
            ctx.AddObjectToAsset(fileName, story);
            ctx.SetMainObject(story);
            AstUtilities.Ctx = ctx;
            AstUtilities.ResetNodeCounter();
            var tokenSoup = new Tokenizer().Tokenize(input);
            var compiler = new Compiler(tokenSoup, story);
            compiler.Make();
        }
    }
}