﻿using System.Collections.Generic;
using System.Text;
using DialogEd.Model;
using DialogEd.Parser;
using UnityEngine;

namespace DialogEd.Editor.DialogEd.Model {
    /// <summary>
    /// Takes tokens and compiles an AST.
    /// </summary>
    public class Compiler {
        private Story owner;
        private Stack<ParsedToken> tokenStack;
        private ParsedToken currentToken;
        private ParsedToken nextToken;
        private ParsedToken lastToken;


        private List<ThreadNode> threads;

        public Compiler(List<ParsedToken> tokens, Story ownerAsset) {
            tokens.Reverse();
            tokenStack = new Stack<ParsedToken>(tokens);
            threads = new List<ThreadNode>();
            owner = ownerAsset;
        }

        public void Make() {
            currentToken = tokenStack.Pop();
            nextToken = tokenStack.Pop();

            var root = BeginThread();

            var node = root;

            while (currentToken.tokenType != TokenType.Invalid) {
                // each input file can have multiple threads in it, make sure this is accounted for
                // with a special case. It seems we have to do it this way because it's basically ending a parsing step
                // and we have to housekeep some pointers.
                if (currentToken.tokenType == TokenType.CloseStory) {
                    threads.Add((ThreadNode)root);
                    Next(TokenType.CloseStory);
                    if (currentToken.tokenType == TokenType.Invalid) {
                        // we have reached the end of this input file
                        break;
                    }

                    root = BeginThread();
                    node = root;
                }

                node = RunThreadStatement(node);
                if (!node) {
                    break;
                }
            }
            
            owner.SetStoryThreads(threads);
        }

        private AstNode RunThreadStatement(AstNode node) {
            Debug.Log($"Running new thread");
            node.Left = Statement();

            if (!node.Left || node.Left.Token.tokenType == TokenType.Invalid) {
                // todo: should analyse the tokens here and see with which one it went derp.
                Debug.LogError($"RunThreadStatement: Unknown / invalid token: {lastToken.tokenType} near {lastToken.value} {currentToken.value} {nextToken.value}");

                return null;
            }

            node.Right = AstUtilities.NewObject<AstNode>(owner);
            node = node.Right;
            return node;
        }

        private AstNode Statement() {
            // at this point, the only valid tokens are:
            // * Identifier (speech, assignments etc)
            // * if
            // * command
            // * goto
            // * global
            // * choice
            // * pre-inc
            // * pre-dec
            
            // in case we start the statement with a global specifier
            // skip it so that AssignmentOrText (which calls Identifier and THAT is interested in the global) 
            // can read it from the last token.
            if (currentToken.tokenType == TokenType.Global) {
                Next(TokenType.Global);
            }
            switch (currentToken.tokenType) {
                case TokenType.Identifier:
                    // as root element, an identifier can only lead into an assignment or into a text.
                    return AssignmentOrText();
                case TokenType.If:
                    return If();
                case TokenType.Command:
                    return Command();
                case TokenType.BeginBranching:
                    return ChoiceBranches();
                case TokenType.Goto:
                    return GoTo();
                // todo: I'll keep this for implementing later.
                // case ETokenType::PlusPlus:
                //     break;
                // case ETokenType::MinusMinus:
                //     break;
                default:
                    Debug.LogError($"Invalid {currentToken.tokenType} found near {lastToken.value} {currentToken.value} {nextToken.value}");
                    return null;
            }
        }

        private AstNode GoTo() {
            // syntax: goto "story thread name"
            Debug.Log("\tStarting GOTO.");
            var gotoToken = currentToken;
            Next(TokenType.Goto);
            // now we're at the string literal for the thread name
            var literal = StringLiteral();
            var node = AstUtilities.NewObject<GotoNode>(owner);
            node.Init(gotoToken, literal);
            Debug.Log($"\tGOTO diverts to {literal.Value}");
            return node;
        }

        private AstNode ChoiceBranches() {
            /*
             * branch node: left: speak node,
             *              right:
             *                  FChoiceNode:    left:   Next choice
             *                                  right:  This choice branch
             *                                  label: choice label
             *
             */
            var branchBase = AstUtilities.NewObject<AstNode>(owner); //new UNode();
            branchBase.Init(currentToken);
            Next(TokenType.BeginBranching);
            // we're now at the spot after the choice token and expect now a speaker and text to speak.
            var speech = (SpeakNode)AssignmentOrText();
            if (!speech) {
                // ... and if it ain't, this is an epic fail
                return null;
            }

            branchBase.Left = speech;
            var node = AstUtilities.NewObject<ChoiceNode>(owner);
            branchBase.Right = node;
            while (currentToken.tokenType != TokenType.EndBranching) {
                Next(TokenType.Branch); // =>
                // now we're at the label node. Which is a Text token
                node.Init(currentToken); // current here is string literal (the choice label)
                Next(TokenType.StringLiteral); // and next should be a thread what happens when selecting the given choice.
                var threadNode = AstUtilities.NewObject<AstNode>(owner);
                node.Left = threadNode;
                bool failed = false;
                while (currentToken.tokenType != TokenType.Branch && currentToken.tokenType != TokenType.EndBranching) {
                    // run until we hit the next branch or the end of the branching
                    threadNode = RunThreadStatement(threadNode);
                    if (!threadNode) {
                        failed = true;
                        break;
                    }
                }
                // / now we oughta be at the branch

                if (failed) {
                    return null;
                }

                // if we have a next branch, swap the node pointer
                if (currentToken.tokenType == TokenType.Branch) {
                    // if so, next next branch node and cycle the current node out.
                    // init for this node happens at the beginning of the while loop.
                    var nextChoice = AstUtilities.NewObject<ChoiceNode>(owner);
                    node.Right = nextChoice;
                    
                    node = nextChoice;
                }
            }

            Next(TokenType.EndBranching);

            return branchBase;
        }

        private CommandNode Command() {
            var cmd = AstUtilities.NewObject<CommandNode>(owner); // new UCommandNode();
            cmd.Init(currentToken);
            Next(TokenType.Command);
            
            // command name cannot be global scoped. that would constitute a syntax error. Identifier will spit an error in this case.
            // now we get identifier lparen [identifier, identifier, ...] rparen
            var cmdName = Identifier();
            cmd.Left = cmdName;
            Next(TokenType.LParen);
            bool failed = false;
            var argNode = AstUtilities.NewObject<AstNode>(owner);
            cmd.Right = argNode;
            while (currentToken.tokenType != TokenType.RParen) {
                // This can be an identifier or a math expression or a logic expression or a number literal or a string literal
                AstNode newArg;
                if (currentToken.tokenType == TokenType.StringLiteral) {
                    newArg = StringLiteral();
                }
                else {
                    // covers number literals and math expressions as well
                    newArg = LogicExpression();
                }

                if (!newArg) {
                    failed = true;
                    break;
                }

                argNode.Left = newArg;
                argNode.Right = AstUtilities.NewObject<AstNode>(owner);
                argNode = argNode.Right;
                // if we're not about to close the command signature, the next token must be a comma as argument separator
                if (currentToken.tokenType != TokenType.RParen) {
                    Next(TokenType.Comma);
                }
            }

            if (failed) {
                return null;
            }

            Next(TokenType.RParen);

            return cmd;
        }

        private StringLiteralNode StringLiteral() {
            Debug.Log($"\tSTRLIT: {currentToken.value}");
            var node = AstUtilities.NewObject<StringLiteralNode>(owner);
            node.Init(currentToken, currentToken.value.Trim('"'));
            Next(TokenType.StringLiteral);
            return node;
        }

        private AstNode If() {
            /*
             * If node:
             * left: Logic Expression
             * right: FNode: left: if true (ThreadNode), right: else (ThreadNode)
             */
            var ifNode = AstUtilities.NewObject<AstNode>(owner);
            ifNode.Init(currentToken);
            Debug.Log("\tStarting IF branch");
            Next(TokenType.If);
            // we're now at the spot after the if, which can only be a logic expression
            var expression = LogicExpression();
            if (!expression) {
                // ... and if it ain't, this is an epic fail
                return null;
            }

            ifNode.Left = expression;
            var leftTrueBranch = AstUtilities.NewObject<AstNode>(owner);
            var ifBranches = AstUtilities.NewObject<AstNode>(owner);
            ifBranches.Left = leftTrueBranch; // this is going to be the true-branch and right is the false branch
            ifNode.Right = ifBranches;

            bool failed = false;

            // now we have to assemble the if branch until we hit either else or endif
            // run the true-branch
            while (currentToken.tokenType != TokenType.Else && currentToken.tokenType != TokenType.Endif) {
                if (currentToken.tokenType == TokenType.Else) {
                    break;
                }

                leftTrueBranch = RunThreadStatement(leftTrueBranch);
                if (!leftTrueBranch) {
                    failed = true;
                    break;
                }
            }

            if (failed) {
                return null;
            }

            // run the else branch if it exists (otherwise RTS will have progressed to the endif)
            if (currentToken.tokenType == TokenType.Else) {
                Debug.Log("\tEntering ELSE branch.");
                Next(TokenType.Else);
                var rightElseBranch = AstUtilities.NewObject<AstNode>(owner);
                ifBranches.Right = rightElseBranch;
                while (currentToken.tokenType != TokenType.Endif) {
                    rightElseBranch = RunThreadStatement(rightElseBranch);
                    if (!rightElseBranch) {
                        failed = true;
                        break;
                    }
                }
            }

            Next(TokenType.Endif);
            Debug.Log("\tENDIF");
            if (failed) {
                return null;
            }

            return ifNode;
        }

        private AstNode LogicExpression() {
            // todo: we could use parenthesis to force operator precedence some day
            var lhs = MathExpression();
            var startExpression = lhs;
            if (!lhs) {
                // LiteralOrIdentifier already spat out error msg
                return null;
            }

            while (CurrentTokenIsComparison() || CurrentTokenIsLogicOp()) {
                // first and
                while (currentToken.tokenType == TokenType.And) {
                    var opToken = currentToken;
                    Next(currentToken.tokenType); // now we're at the RHS of the op
                    var rhs = MathExpression();
                    if (!rhs) {
                        return null;
                    }

                    var tmp = AstUtilities.NewObject<BinOpNode>(owner);
                    tmp.Init(opToken, lhs, rhs);
                    lhs = tmp;
                }

                // then or
                while (currentToken.tokenType == TokenType.Or) {
                    var opToken = currentToken;
                    Next(currentToken.tokenType); // now we're at the RHS of the op
                    var rhs = MathExpression();
                    if (!rhs) {
                        return null;
                    }

                    var tmp = AstUtilities.NewObject<BinOpNode>(owner);
                    tmp.Init(opToken, lhs, rhs);
                    lhs = tmp;
                }

                // Then check for comparison operations and put them also in a binop
                // and now check additions, because dots before dashes.
                while (CurrentTokenIsComparison()) {
                    var opToken = currentToken;
                    Next(currentToken.tokenType); // now we're at the RHS of the op
                    var rhs = MathExpression();
                    if (!rhs) {
                        return null;
                    }

                    var tmp = AstUtilities.NewObject<BinOpNode>(owner);
                    tmp.Init(opToken, lhs, rhs);
                    lhs = tmp;
                }
            }

            // this handles stuff like "if someVarName" and evaluates if that is true (> 0)
            if (lhs == startExpression) {
                // if lhs is number literal make the binop a literal
                // if lhs is identifier, make it a Variable
                // if lhs is anything else than that (for instance an operator) it will throw syntax errors before we get here.
                var tmp = AstUtilities.NewObject<BinOpNode>(owner);
                tmp.Init(new ParsedToken() {
                    tokenType = lhs.Token.tokenType
                }, startExpression, null);
                lhs = tmp;
            }

            return lhs;
        }

        private AstNode AssignmentOrText() {
            Debug.Log($"AssignmentOrText: {currentToken.tokenType} {nextToken.tokenType} ({currentToken.value} {nextToken.value})");
            // this will get the identifier however shaped (normal or with type info)
            // it will, in any case, strip the colon if there is one.
            var identifier = Identifier();

            // speaker: "Speaking some text"
            if (currentToken.tokenType == TokenType.StringLiteral) {
                return DialogueMessage(identifier);
            }

            // varName = [...]
            if (currentToken.tokenType == TokenType.SingleEqual) {
                // account for successive assignments
                return Assignment(identifier);
            }

            // todo: this can also be a post increment or post decrement, might implement later
            Debug.LogError($"AssignmentOrText: Syntax Error: {currentToken.tokenType} {nextToken.tokenType} ({currentToken.value} {nextToken.value})");

            Next(currentToken.tokenType);
            return null;
        }

        private AstNode Assignment(IdentifierNode lhsIdentifier) {
            bool hadRun = false;
            AstNode result = lhsIdentifier;
            while (currentToken.tokenType == TokenType.SingleEqual && result.Token.tokenType == TokenType.Identifier) {
                // assignment
                // now, we know the left hand side, it's an identifier, has to be for an assignment.
                // but the RHS could be a number literal, or another identifier or a whole expression.
                Next(TokenType.SingleEqual); // current is now at token after =
                hadRun = true;

                var tmpBinOp = AstUtilities.NewObject<BinOpNode>(owner); // new UAssignmentNode(lastToken, lhsIdentifier, MathExpression());
                Debug.Log($"Assignment {lhsIdentifier.IdentifierLabel} = {currentToken.tokenType}");
                // needed because MathExpression advances through the stack and causes lastToken to change before its copied into the Init function
                var tmpLastToken = lastToken;
                tmpBinOp.Init(tmpLastToken, result, MathExpression());
                result = tmpBinOp;
                Debug.Log($"Assignment done. Current token now {currentToken.tokenType}");
            }

            if (!hadRun) {
                Debug.LogError($"Syntax Error. Expected Assignment but got {currentToken.tokenType}");
                Next(currentToken.tokenType);
                return null;
            }

            // last MathExpression already called Next()
            return result;
        }

        private AstNode MathExpression() {
            // todo: we could use parenthesis to force operator precedence some day
            AstNode lhs = NumberLiteralOrIdentifier();
            if (!lhs) {
                // LiteralOrIdentifier already spat out error msg
                return null;
            }

            while (CurrentTokenIsBinOp()) {
                // check multiplication and division first
                while (currentToken.tokenType == TokenType.Asterisk || currentToken.tokenType == TokenType.Slash) {
                    var opToken = currentToken;
                    Next(currentToken.tokenType); // now we're at the RHS of the math op
                    // expression should handle identifier and number literals
                    var rhs = NumberLiteralOrIdentifier();
                    if (!rhs) {
                        return null;
                    }

                    var tmp = AstUtilities.NewObject<BinOpNode>(owner);
                    tmp.Init(opToken, lhs, rhs);
                    lhs = tmp;
                }

                // and now check additions, because dots before dashes.
                while (currentToken.tokenType == TokenType.Plus || currentToken.tokenType == TokenType.Minus) {
                    var opToken = currentToken;
                    Next(currentToken.tokenType); // now we're at the RHS of the math op
                    // expression should handle identifier and number literals
                    var rhs = NumberLiteralOrIdentifier();
                    if (!rhs) {
                        return null;
                    }

                    var tmp = AstUtilities.NewObject<BinOpNode>(owner);
                    tmp.Init(opToken, lhs, rhs);
                    lhs = tmp;
                }
            }

            return lhs;
        }

        private AstNode NumberLiteralOrIdentifier() {
            // valid tokens here are: 
            // * number literal
            // * identifier
            if (currentToken.tokenType == TokenType.NumberLiteral) {
                Debug.Log($"\tNUMLIT: {currentToken.value}");
                var t = AstUtilities.NewObject<NumberLiteralNode>(owner); // new UNumberNode(currentToken);
                t.Init(currentToken);
                Next(TokenType.NumberLiteral);
                return t;
            }

            if (currentToken.tokenType == TokenType.Identifier) {
                return Identifier(); // calls Next() within
            }

            if (currentToken.tokenType == TokenType.Global) {
                Next(TokenType.Global);
                return Identifier();
            }

            Debug.LogError($"Syntax Error: Expected number literal, identifier or global specifier. Got {currentToken.tokenType}");
            Next(currentToken.tokenType);
            return null;
        }

        private AstNode DialogueMessage(IdentifierNode lhsIdentifier) {
            var idNode = lhsIdentifier;
            if (!idNode) {
                Debug.LogError("Syntax error: Expected Identifier before Text, got null value");
                Next(currentToken.tokenType);
                return null;
            }

            StringBuilder inValue = new StringBuilder(currentToken.value.Trim('"'));
            ParsedToken textBaseToken = currentToken;
            Next(TokenType.StringLiteral);
            // thing is, it is syntactically allowed to have another text token here that is supposed to be merged with the previous one.
            while (currentToken.tokenType == TokenType.StringLiteral) {
                inValue.AppendLine(currentToken.value.Trim('"'));
                Next(TokenType.StringLiteral);
            }

            var speakNode = AstUtilities.NewObject<SpeakNode>(owner);
            var textNode = AstUtilities.NewObject<StringLiteralNode>(owner);
            textNode.Init(textBaseToken, inValue.ToString());
            speakNode.Init(idNode, textNode);

            return speakNode;
        }

        private AstNode BeginThread() {
            Debug.Log("Opening new Story Thread");

            var node = AstUtilities.NewObject<ThreadNode>(owner);

            node.Init(currentToken);
            Next(TokenType.OpenStory);
            return node;
        }

        private IdentifierNode Identifier() {
            var globalIdentifier = lastToken.tokenType == TokenType.Global;
            IdentifierNode identifier = AstUtilities.NewObject<IdentifierNode>(owner);
            identifier.Init(currentToken);
            var node = identifier;
            // if next token it's a colon: this could be a variable type definition (i:inventoryItem).
            // or it could be a speaker name (john: "speak")
            if (nextToken.tokenType == TokenType.Colon) {
                Next(TokenType.Identifier); // now current is colon

                Next(TokenType.Colon); // and now current is the token after colon
                if (currentToken.tokenType == TokenType.Identifier) {
                    node = AstUtilities.NewObject<IdentifierNode>(owner);
                    node.Init(currentToken, identifier);
                    Next(currentToken.tokenType);
                }
            }
            else {
                Next(TokenType.Identifier);
            }

            // otherwise this is some kind of binary operator or comparison operator or assignment.
            // either way, this will be evaluated at the callsite.
            string scope = globalIdentifier ? "g" : "l";
            Debug.Log($"\tIDENTIFIER: {scope} {node.IdentifierLabel}");
            node.SetScope(globalIdentifier);
            return node;
        }

        private void Next(TokenType consume) {
            if (currentToken.tokenType != consume) {
                Debug.LogError($"Next: Unexpected token! Found {currentToken.tokenType}, expected {consume}");
                return;
            }

            lastToken = currentToken;
            currentToken = nextToken;
            if (tokenStack.Count > 0) {
                nextToken = tokenStack.Pop();
            }
            else {
                nextToken = new ParsedToken(); // becomes invalid token
            }

            // Ignore comments, of course
            if (currentToken.tokenType == TokenType.Comment) {
                Next(TokenType.Comment);
            }

            if (currentToken.tokenType == TokenType.CommentMultiLine) {
                Next(TokenType.CommentMultiLine);
            }
        }

        private bool CurrentTokenIsBinOp() {
            return currentToken.tokenType == TokenType.Plus ||
                   currentToken.tokenType == TokenType.Minus ||
                   currentToken.tokenType == TokenType.Slash ||
                   currentToken.tokenType == TokenType.Asterisk;
        }

        private bool CurrentTokenIsComparison() {
            return currentToken.tokenType == TokenType.Greater ||
                   currentToken.tokenType == TokenType.GreaterOrEqual ||
                   currentToken.tokenType == TokenType.Less ||
                   currentToken.tokenType == TokenType.LessOrEqual ||
                   currentToken.tokenType == TokenType.Equal ||
                   currentToken.tokenType == TokenType.NotEqual;
        }

        private bool CurrentTokenIsLogicOp() {
            return currentToken.tokenType == TokenType.And ||
                   currentToken.tokenType == TokenType.Or;
        }
    }
}