﻿using DialogEd.Model;
using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

namespace DialogEd.Editor.DialogEd {
    public static class AstUtilities {
        private static int nodeCounter;

        public static AssetImportContext Ctx;

        public static void ResetNodeCounter() {
            nodeCounter = 0;
        }
        
        public static T NewObject<T>(Story owner) where T : AstNode {
            var node = ScriptableObject.CreateInstance<T>();
            node.name = $"{typeof(T).Name}_{nodeCounter++}";
            Ctx.AddObjectToAsset(node.name, node);
            return node;
        }
    }
}